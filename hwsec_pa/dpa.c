/**********************************************************************************
Copyright Institut Telecom
Contributors: Renaud Pacalet (renaud.pacalet@telecom-paristech.fr)

This software is a computer program whose purpose is to experiment timing and
power attacks against crypto-processors.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms. For more
information see the LICENCE-fr.txt or LICENSE-en.txt files.
**********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <math.h>

#include <tr_pcc.h>
#include <utils.h>
#include <traces.h>
#include <des.h>

/* The P permutation table, as in the standard. The first entry (16) is the
 * position of the first (leftmost) bit of the result in the input 32 bits word.
 * Used to convert target bit index into SBox index (just for printed summary
 * after attack completion). */
int p_table[32] = {
  16, 7, 20, 21,
  29, 12, 28, 17,
  1, 15, 23, 26,
  5, 18, 31, 10,
  2, 8, 24, 14,
  32, 27, 3, 9,
  19, 13, 30, 6,
  22, 11, 4, 25
};

tr_context ctx;                 /* Trace context (see traces.h) */

int nBest = 2;
int nBytes = 8;
int nBits = 6;

int best_guess[8][2];                 /* Best guess */
int best_idx[8][2];                   /* Best argmax */
float best_max[8][2];                 /* Best max sample value */



/* A function to allocate cipher texts and power traces, read the
 * datafile and store its content in allocated context. */
void read_datafile (char *name, int n);

/* Compute the average power trace of the traces context ctx, print it in file
 * <prefix>.dat and print the corresponding gnuplot command in <prefix>.cmd. In
 * order to plot the average power trace, type: $ gnuplot -persist <prefix>.cmd
 * */
void average (char *prefix);

/* Decision function: takes a ciphertext and returns an array of 64 values for
 * an intermediate DES data, one per guess on a 6-bits subkey. In this example
 * the decision is the computed value of bit index <target_bit> of L15. Each of
 * the 64 decisions is thus 0 or 1.*/
void decision (uint64_t ct, int s, int hd[64]);

/* Apply P. Kocher's DPA algorithm based on decision function. Computes 64 DPA
 * traces dpa[0..63], best_guess (6-bits subkey corresponding to highest DPA
 * peak), best_idx (index of sample with maximum value in best DPA trace) and
 * best_max (value of sample with maximum value in best DPA trace). */
void dpa_attack (void);

int
main (int argc, char **argv)
{
  int n;                        /* Number of experiments to use. */
  char *key_bits[8] = {"1-6", "7-12", "13-18", "19-24", "25-30", "31-36", "37-42", "43-48"};
  int sbox_idx[8] = {0,0,0,0,0,0,0,0};

  int i = 0;
  int j = 0;
  uint64_t key;    /* 64 bits secret key */
  uint64_t ks[16]; /* Key schedule (array of 16 round keys) */
  uint64_t subkey = 0;
  /************************************************************************/
  /* Before doing anything else, check the correctness of the DES library */
  /************************************************************************/
  if (!des_check ())
    {
      ERROR (-1, "DES functional test failed");
    }

  /*************************************/
  /* Check arguments and read datafile */
  /*************************************/
  /* If invalid number of arguments (including program name), exit with error
   * message. */
  if (argc != 3)
    {
      ERROR (-1, "usage: dpa <file> <n> <b>\n  <file>: name of the traces file in HWSec format\n          (e.g. /datas/teaching/courses/HWSec/labs/data/HWSecTraces10000x00800.hws)\n  <n>: number of experiments to use\n");
    }
  /* Number of experiments to use is argument #2, convert it to integer and
   * store the result in variable n. */
  n = atoi (argv[2]);
  if (n < 1)                    /* If invalid number of experiments. */
    {
      ERROR (-1, "invalid number of experiments: %d (shall be greater than 1)", n);
    }

  /* Read power traces and ciphertexts. Name of data file is argument #1. n is
   * the number of experiments to use. */
  read_datafile (argv[1], n);

   /*****************************************************************************
   * Compute and print average power trace. Store average trace in file
   * "average.dat" and gnuplot command in file "average.cmd". In order to plot
   * the average power trace, type: $ gnuplot -persist average.cmd
   *****************************************************************************/
  average ("average"); 
  
    /***************************************************************
     * Attack target bit in L15=R14 with P. Kocher's DPA technique *
     ***************************************************************/
    dpa_attack ();

    /*****************
     * Print summary *
     *****************/
    int w = 18;
    printf ("%*s%*s%*s%*s\n", w, "key bits", w, "best guess", w, "max pcc", w, "max index");
    for (i = 0; i < nBytes; i++) /* for every sbox */
    {
      for (j = 0; j < nBest/2; j++)
      {
        printf ("%*s    (0x%02x),%2d (0x%02x)%2d %10.2f,%5.2f%10d,%3d\n", w, key_bits[i], 
          best_guess[i][2*j], best_guess[i][2*j], 
          best_guess[i][2*j+1], best_guess[i][2*j+1],
          best_max[i][2*j], best_max[i][2*j+1], 
          best_idx[i][2*j],best_idx[i][2*j+1]);        
      }
      /*printf("print end\n");*/
    }

    /*printf("check if the round key is right\n");*/
    
    key = tr_key (ctx); /* Extract 64 bits secret key from context */
    des_ks (ks, key);   /* Compute key schedule */
    /*printf ("the right round key is 0x%llx\n", ks[15]);*/
    
    for (i = 0; i < nBytes; i++){
      sbox_idx[i] = 0;
    }     

    for (sbox_idx[0] = 0;  sbox_idx[0] < nBest; sbox_idx[0]++){
      for (sbox_idx[1] = 0; sbox_idx[1] < nBest; sbox_idx[1]++){
        for (sbox_idx[2] = 0; sbox_idx[2] < nBest; sbox_idx[2]++){
          for (sbox_idx[3] = 0; sbox_idx[3] < nBest; sbox_idx[3]++){
            for (sbox_idx[4] = 0; sbox_idx[4] < nBest; sbox_idx[4]++){
              for (sbox_idx[5] = 0; sbox_idx[5] < nBest; sbox_idx[5]++){
                for (sbox_idx[6] = 0; sbox_idx[6] < nBest; sbox_idx[6]++){
                  for (sbox_idx[7] = 0; sbox_idx[7] < nBest; sbox_idx[7]++){
                    subkey = 
                              ((uint64_t)best_guess[0][sbox_idx[0]] << ((nBytes-0-1) * nBits))
                              | ((uint64_t)best_guess[1][sbox_idx[1]] << ((nBytes-1-1) * nBits))
                              | ((uint64_t)best_guess[2][sbox_idx[2]] << ((nBytes-2-1) * nBits))
                              | ((uint64_t)best_guess[3][sbox_idx[3]] << ((nBytes-3-1) * nBits))
                              | ((uint64_t)best_guess[4][sbox_idx[4]] << ((nBytes-4-1) * nBits))
                              | ((uint64_t)best_guess[5][sbox_idx[5]] << ((nBytes-5-1) * nBits))
                              | ((uint64_t)best_guess[6][sbox_idx[6]] << ((nBytes-6-1) * nBits))
                              | ((uint64_t)best_guess[7][sbox_idx[7]] << ((nBytes-7-1) * nBits));

                    if (subkey == ks[15]) 
                    {
                      /* If guessed 16th round key matches actual 16th round key */
                      printf ("We got it!!! the secret key is 0x%llx\n", subkey); /* Cheers */

                      tr_free (ctx);                /* Free traces context */
                      return 0;                     /* Exits with "everything went fine" status. */
                    }                    
                  }
                }
              }
            }  
          }
        }
      }
    }
  
  tr_free (ctx);                /* Free traces context */
  return 0;                     /* Exits with "everything went fine" status. */
}

void
read_datafile (char *name, int n)
{
  int tn;

  ctx = tr_init (name, n);
  tn = tr_number (ctx);
  if (tn != n)
    {
      tr_free (ctx);
      ERROR (-1, "Could not read %d experiments from traces file. Traces file contains %d experiments.", n, tn);
    }
}

void
average (char *prefix)
{
  int i;                        /* Loop index */
  int n;                        /* Number of traces. */
  float *sum;                   /* Power trace for the sum */
  float *avg;                   /* Power trace for the average */

  n = tr_number (ctx);          /* Number of traces in context */
  sum = tr_new_trace (ctx);     /* Allocate a new power trace for the sum. */
  avg = tr_new_trace (ctx);     /* Allocate a new power trace for the average. */
  tr_init_trace (ctx, sum, 0.0);        /* Initialize sum trace to all zeros. */
  for (i = 0; i < n; i++)       /* For all power traces */
    {
      tr_acc (ctx, sum, tr_trace (ctx, i));     /* Accumulate trace #i to sum */
    }                           /* End for all power traces */
  /* Divide trace sum by number of traces and put result in trace avg */
  tr_scalar_div (ctx, avg, sum, (float) (n));
  tr_plot (ctx, prefix, 1, -1, &avg);
  printf ("Average power trace stored in file '%s.dat'. In order to plot it, type:\n", prefix);
  printf ("$ gnuplot -persist %s.cmd\n", prefix);

  tr_free_trace (ctx, sum);     /* Free sum trace */
  tr_free_trace (ctx, avg);     /* Free avg trace */
  return;
}

void
decision (uint64_t ct, int s, int hd[64]) /* compute the hamming distance between r14 and r15 */
{
  int g;                        /* Guess */
  uint64_t r16l16;              /* R16|L16 (64 bits state register before final permutation) */
  uint64_t l16;                 /* L16 (as in DES standard) */
  uint64_t r16;                 /* R16 (as in DES standard) */
  uint64_t er15;                /* E(R15) = E(L16) */
  uint64_t rk;                  /* Value of last round key */

  uint64_t r15;                 /* L15 (as in DES standard) */
  uint64_t invp_l15;            /* inverse of P permutation of l15, L15 = R14 */

  r16l16 = des_ip (ct);         /* Compute R16|L16 */
  l16 = des_right_half (r16l16);        /* Extract right half */
  r16 = des_left_half (r16l16); /* Extract left half */
  er15 = des_e (l16);           /* Compute E(R15) = E(L16) */
  r15 = l16;

  /* For all guesses (64). rk is a 48 bits last round key with all 6-bits
   * subkeys equal to current guess g (nice trick, isn't it?). */
  for (g = 0, rk = UINT64_C (0); g < 64; g++, rk += UINT64_C (0x041041041041))
  {
    invp_l15 = des_n_p (r16) ^ (des_sboxes (er15 ^ rk));       /* Compute L15 deducing scatter */
    hd[g] = hamming_distance (
      (invp_l15 >> (4*(nBytes-s-1))) & UINT64_C(15), 
      (des_n_p (l16) >> (4*(nBytes-s-1))) & UINT64_C(15)
    ); /* calculate hamming distance of r15 and r14 */
  } /* End for guesses */
  return;
}

void
dpa_attack (void)
{
  int i;                        /* Loop index */
  int s;                        /* index of sbox */
  int n;                        /* Number of traces. */
  int l;                        /* number of points per trace */ 
  int g;                        /* Guess on a 6-bits subkey */
  int idx = 0;;                 /* Argmax (index of sample with maximum value in a trace) */

  float max = 0.0;              /* Max sample value in a trace */

  uint64_t ct;                  /* Ciphertext */
  int hd[64];                   /* hamming distance on every key guess */
  
  tr_pcc_context pcc_ctx;

  float *t;                     /* Power trace */
  float *pcc[64];
 
  n = tr_number (ctx);          /* Number of traces in context */
  l = tr_length (ctx);          /* number of points per trace */ 
 
  /* todo: for all sboxes */
  for (s = 0; s < nBytes; s++)
  {
    /*printf ("init pcc ctx ======= %d\n", s);*/
    pcc_ctx = tr_pcc_init(l, 64);

    /*printf("insert =======\n");*/
    for (i = 0; i < n; i++)       /* For all experiments */
    {
      /*printf("get power trace====\n");*/
      t = tr_trace (ctx, i);    /* Get power trace */
      /*printf("insert x====\n");*/
      tr_pcc_insert_x(pcc_ctx, t);

      ct = tr_ciphertext (ctx, i);      /* Get ciphertext */

      /*printf("get hamming distance=====%d,%d\n", s, i);*/
      decision (ct, s, hd);         
      /* For all guesses (64) */
      for (g = 0; g < 64; g++)
      { 
        /*printf("insert y===sbox:%d, guess:%d, %d\n", s, g, hd[g]); */
        tr_pcc_insert_y(pcc_ctx, g, hd[g]);
      }                       /* End for guesses */
    } /* End for experiments */

    /*printf("consolidate pcc ctx======\n");*/
    tr_pcc_consolidate(pcc_ctx);  

    best_guess[s][0] = 0;               /* Initialize best guess */
    best_max[s][0] = 0.0;               /* Initialize best maximum sample */
    best_idx[s][0] = 0;                 /* Initialize best argmax (index of maximum sample) */

    best_guess[s][1] = 0;               /* Initialize best guess */
    best_max[s][1] = 0.0;               /* Initialize best maximum sample */
    best_idx[s][1] = 0;                 /* Initialize best argmax (index of maximum sample) */

    for(g = 0; g < 64; g++) 
    {
      /*printf("get pcc======\n");*/
      pcc[g] = tr_pcc_get_pcc(pcc_ctx, g);

      /*for (i = 0; i < l; i++)
      {
        if (i < 550 || i > 625)
        {
          pcc[g][i] = 0.0;         
        }
      }*/
      
      /* pick two maximum dpa */
      max = tr_max (ctx, pcc[g], &idx); /* Get max and argmax of DPA trace */

      if (max > best_max[s][0] || g == 0)     /* If better than current best max (or if first guess) */
      {
        best_max[s][1] = best_max[s][0];
        best_idx[s][1] = best_idx[s][0];
        best_guess[s][1] = best_guess[s][0];  

        best_max[s][0] = max;       /* Overwrite best max with new one */
        best_idx[s][0] = idx;       /* Overwrite best argmax with new one */
        best_guess[s][0] = g;       /* Overwrite best guess with new one */        
      }
    }

    tr_pcc_free(pcc_ctx);
  } /* end of sboxes */

  return;
}
